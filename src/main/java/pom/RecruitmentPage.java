package pom;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static java.time.Duration.ofSeconds;

public class RecruitmentPage {

    public static String url = "https://opensource-demo.orangehrmlive.com/web/index.php/recruitment/addJobVacancy";
    //selectori
    private final WebDriver driver;
    public By enterVacancyName = By.xpath("//div[@class='oxd-input-group oxd-input-field-bottom-space']//input[@class='oxd-input oxd-input--active']");
    public By enterDescription = By.xpath("//textarea[@class=\"oxd-textarea oxd-textarea--active oxd-textarea--resize-vertical\"]");
    public By dropdownHiringManger = By.xpath("//input[@placeholder='Type for hints...']");
    public By dropdownJobTitle = By.xpath("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div/form/div[1]/div[2]/div/div[2]/div/div/div[2]/i");
    //    public By optionJobTitle = By.xpath("//div[@class='oxd-select-wrapper']//div[@role='listbox']//div");
    public By optionJobTitle = By.xpath("//div[@role='listbox']//span");
    public By enterNumberOfPositions = By.xpath("//*[@class='oxd-grid-2 orangehrm-full-width-grid']//*[@class='oxd-input oxd-input--active']['data-v-1f99f73c']");
    public By saveButtonVacancyPage = By.xpath("//button[@type='submit']");
    public By confirmationMessageVacancyPage = By.xpath("//*[@id=\"oxd-toaster_1\"]");
    public By linkFeedURL = By.xpath("//a[@href='https://opensource-demo.orangehrmlive.com/web/index.php/recruitmentApply/jobs.rss']");
    public By linkPageURL = By.xpath("//a[@href='https://opensource-demo.orangehrmlive.com/web/index.php/recruitmentApply/jobs.html']");

    //constructori

    public RecruitmentPage(WebDriver driver) {
        this.driver = driver;
    }

    //actiuni
    public void enterVacancyName(String vacancyName) {
        driver.findElement(enterVacancyName).sendKeys(vacancyName);
    }

    public void enterDescription(String string) {
        driver.findElement(enterDescription).sendKeys(string);
    }

    public Boolean selectJobTitle(String string) {
        WebElement dropdown = driver.findElement(dropdownJobTitle);
        dropdown.click();
        List<WebElement> allOptions = driver.findElements(optionJobTitle);

        WebDriverWait explicitWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        Actions action = new Actions(driver);
        boolean found = false;
        for (WebElement e : allOptions) {
            String currentOption = e.getText();
            action.moveToElement(e).perform();
            if (currentOption.contains(string)) {
                found = true;
                explicitWait.until(ExpectedConditions.elementToBeClickable(e)).click();
                break;
            }
        }
        return found;
    }

    public boolean selectHiringManager() throws InterruptedException {
        WebElement searchHiringManager = driver.findElement(dropdownHiringManger);
        searchHiringManager.sendKeys("Manager1");

        Thread.sleep(5000);

        searchHiringManager.sendKeys(Keys.ARROW_DOWN);
        searchHiringManager.sendKeys(Keys.ENTER);
        return searchHiringManager.isEnabled();
    }

    public void enterNumberOfOpenedPositions(String number) {
        driver.findElement(enterNumberOfPositions).sendKeys(number);
    }

    public void saveButtonVacancyPage() {
        driver.findElement(this.saveButtonVacancyPage).click();
    }

    public boolean confirmationMessageVacancyPage() {
        return driver.findElement(confirmationMessageVacancyPage).isDisplayed();
    }

    public void clickOnLinkFeedURL() {
        driver.findElement(linkFeedURL).click();
        driver.getWindowHandles().forEach(tab -> driver.switchTo().window(tab));
    }

    public void clickOnLinkPageURL() {
        driver.getWindowHandles().forEach(tab -> driver.switchTo().window(tab));
        driver.findElement(linkPageURL).click();
        driver.getWindowHandles().forEach(tab -> driver.switchTo().window(tab));
    }
}




