package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class VacanciesPage {
    public static String url = "https://opensource-demo.orangehrmlive.com/web/index.php/recruitment/viewJobVacancy";
    // public By dropdownJobTitleOptions = By.xpath("//div[@class='oxd-select-wrapper']//div[@class='oxd-select-text oxd-select-text--active']");
    public By dropdownJobTitleOptions = By.xpath("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[1]/div[2]/form/div[1]/div/div[1]/div/div[2]/div/div/div[2]/i");
    //    public By dropdownJobTitleOptions = By.xpath("(//div[@class='oxd-select-text--after'])[1]");
//    public By searchByJobTitle = By.xpath("//div[@role='listbox']//div");
//    public By searchByJobTitle = By.xpath("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[1]/div[2]/form/div[1]/div/div[1]/div/div[2]/div/div[2]/div[20]/span");
//    public By searchByJobTitle = By.xpath("(//div[@role='option'])[19]");
    public By searchByJobTitle = By.xpath("//div[@role='listbox']//span");
    public By searchButton = By.xpath("//button[@type='submit']");
    public By checkBoxCheckAll = By.xpath("//div[@role='columnheader']//i[@class='oxd-icon bi-check oxd-checkbox-input-icon']");
    public By deleteSelectedButton = By.cssSelector("button[class='oxd-button oxd-button--medium oxd-button--label-danger orangehrm-horizontal-margin']");
    public By deleteConfirmationButton = By.xpath("//div[@role='document']//*[normalize-space()='Yes, Delete']");
    public By infoMessage = By.xpath("//div[@class='oxd-toast-content oxd-toast-content--info']");
    public By vacanciesTableRows = By.xpath("//div[@role='table']//div[@role='row']");
    private WebDriver driver;

    public VacanciesPage(WebDriver driver) {
        this.driver = driver;
    }

    public Boolean searchByAJobTitlePosition(String string) {
        WebElement dropdown = driver.findElement(dropdownJobTitleOptions);
        dropdown.click();
        List<WebElement> allOptions = driver.findElements(searchByJobTitle);

        WebDriverWait explicitWait = new WebDriverWait(driver, Duration.ofSeconds(5));
        Actions action = new Actions(driver);
        boolean found = false;
        for (WebElement e : allOptions) {
            String currentOption = e.getText();
            action.moveToElement(e).perform();
            if (currentOption.contains(string)) {
                found = true;
                explicitWait.until(ExpectedConditions.elementToBeClickable(e)).click();
                break;
            }
        }
        return found;
    }

    public void clickOnSearchButton() {
        driver.findElement(this.searchButton).click();
    }

    public boolean clickOnCheckBoxAll() {
        if (!driver.findElement(this.checkBoxCheckAll).isSelected()) {
            driver.findElement(this.checkBoxCheckAll).click();
        }
        return true;
    }

    public boolean clickOnDeleteSelectedButton() {
        if (driver.findElement(this.deleteSelectedButton).isDisplayed()) {
            driver.findElement(this.deleteSelectedButton).click();
        }
        return true;
    }

    public void clickOnDeleteConfirmationButton() {
        driver.findElement(this.deleteConfirmationButton).click();
    }

    public Boolean searchInVacanciesGrid(String string) {
        boolean found = false;
        List<WebElement> allHeaders = driver.findElements(vacanciesTableRows);
        for (WebElement e : allHeaders) {
            String searchedValue = e.getText();
            if (searchedValue.contains(string)) {
                found = true;
                break;
            }
        }
        return found;
    }

    public boolean displayInfoMessage() {
        return driver.findElement(this.infoMessage).isDisplayed();
    }

}
