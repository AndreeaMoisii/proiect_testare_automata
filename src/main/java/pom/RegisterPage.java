package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class RegisterPage {
    public static String url = "https://opensource-demo.orangehrmlive.com/web/index.php/pim/addEmployee";
    public By firstname = By.xpath("//input[@name='firstName']");
    public By lastname = By.cssSelector("input[name='lastName']");
    public By addPictureButton = By.cssSelector("[class='employee-image-wrapper'] + button[class='oxd-icon-button oxd-icon-button--solid-main employee-image-action'][type='button']");
    public By inputPicture = By.xpath("//input[@type='file' and @class='oxd-file-input']");
    public By warningMessageForPicture = By.xpath("//span[@class=\"oxd-text oxd-text--span oxd-input-field-error-message oxd-input-group__message\"][text()=\"Attachment Size Exceeded\"]");
    public By switchCreateLoginDetails = By.xpath("//*[@class='oxd-form-row user-form-header']//*[@class='oxd-switch-wrapper']//span['data-v-8e4757dc']");
    public By enterNewUsername = By.xpath("//div[@class=\"oxd-form-row\"]//div[\"data-v-957b4417\"]/input[@class=\"oxd-input oxd-input--active\"][@autocomplete=\"off\"][\"data-v-1f99f73c\"]");
    public By disabledStatus = By.cssSelector("input[type=\"radio\"][value=\"2\"] + span[class=\"oxd-radio-input oxd-radio-input--active --label-right oxd-radio-input\"]");
    public By saveButton = By.xpath("//button[@type='submit']");
    public By confirmationMessage = By.xpath("//*[@id='oxd-toaster_1']");

    //selectori
    private WebDriver driver;
    private String filePath = "C:\\Projects\\proiect_testare_automata\\src\\test\\resources\\image 4k.jpg";

    //private String filepath="C:\\Projects\\proiect_testare_automata\\src\\test\\resources\\2PNG.png";

    //constructori

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    //actiuni

    public void enterFirstname(String firstname) {
        driver.findElement(this.firstname).sendKeys(firstname);
    }

    public void enterLastname(String lastname) {
        driver.findElement(this.lastname).sendKeys(lastname);
    }

    public void switchOnCreateLoginDetails() {
        if (!driver.findElement(this.switchCreateLoginDetails).isSelected()) {
            driver.findElement(switchCreateLoginDetails).click();
        }
    }

    public void switchOffCreateLoginDetails() {
        driver.findElement(switchCreateLoginDetails).click();
    }

    public void enterNewUsername(String username) {
        driver.findElement(this.enterNewUsername).sendKeys(username);
    }

    public boolean selectDiabledStatus() {
        if (!driver.findElement(this.disabledStatus).isSelected()) {
            driver.findElement(this.disabledStatus).click();
        }
        return true;
    }

    public String newUsernameIsVisible() {
        return driver.findElement(this.enterNewUsername).getAttribute("value");
    }

    public void clickOnSaveButton() {
        driver.findElement(this.saveButton).click();
    }

    public boolean confirmationMessage() {
        driver.findElement(confirmationMessage).isDisplayed();
        return true;
    }

    public void addPictureButton() {
        driver.findElement(this.addPictureButton).click();
    }

    public void uploadPicture() throws InterruptedException {
        driver.findElement(this.inputPicture).sendKeys(filePath);
        Thread.sleep(2000);
    }

    public String warningMessageForPicture() {
        return driver.findElement(this.warningMessageForPicture).getText();
    }

}
