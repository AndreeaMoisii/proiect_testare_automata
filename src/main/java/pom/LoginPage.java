package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    public static String url = "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login";
    // selectori
    private final WebDriver driver;
    public By username = By.xpath("//*[@name='username']");
    public By password = By.xpath("//input[@class = 'oxd-input oxd-input--active' and @name='password']");
    public By loginButton = By.xpath("//button[@type = 'submit']");
    public By userArea = By.xpath("//div[@class='oxd-topbar-header-userarea']");
    public By loginErrorMessage = By.xpath("//*[@class=\"oxd-alert oxd-alert--error\"]");

    //constructori

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    //actiuni
    public void enterUsername(String username) {
        driver.findElement(this.username).sendKeys(username);
    }

    public void enterPassword(String password) {
        driver.findElement(this.password).sendKeys(password);
    }

    public void clickLoginButton() {
        driver.findElement(this.loginButton).click();
    }

    public boolean userAreaIsDisplayed() {
        return driver.findElement(this.userArea).isDisplayed();
    }

    public boolean errorMessageIsDisplayed() {
        return driver.findElement(this.loginErrorMessage).isDisplayed();
    }

}
