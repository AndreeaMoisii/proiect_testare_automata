package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        dryRun = true,
        features = "src/test/java/features",
        glue = "stepdefinition",
        tags = "@Positive",
        plugin = {"pretty", "html:target/PositiveTests.html"}
)

public class PositiveTestsRunners {

}
