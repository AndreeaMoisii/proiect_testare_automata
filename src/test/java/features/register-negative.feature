@UI@Negative
Feature: Register negative test scenarios

  Background: Login with valid credentials
    Given Be on Login page
    When Enter username "Admin"
    And Enter password "admin123"
    Then Click on Login button

  Scenario: Upload profile picture that exceeds the maximum size allowed
    Given Add Employee page is opened
    When Click on Add picture button
    And Select a picture that exceeds the maximum size allowed
    Then Warning message is displayed
