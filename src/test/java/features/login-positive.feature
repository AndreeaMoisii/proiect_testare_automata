@UI@Positive
Feature: Login positive test scenario

  Scenario Outline: Login with valid credentials
    Given Be on Login page
    When Enter username "<username>"
    And Enter password "<password>"
    And Click on Login button
    Then User area is displayed
    Examples:
      | username | password |
      | Admin    | admin123 |