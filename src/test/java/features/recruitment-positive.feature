@UI@Positive
Feature: Recruitment positive test scenarios

  Background:
    Given Be on Login page
    When Enter username "Admin"
    And Enter password "admin123"
    And Click on Login button
    And Add Employee page is opened
    And Enter firstname "Manager3"
    And Enter lastname "Manager1"
    Then Click on Save button
#TO check later
  Scenario Outline: Create new vacancy position with valid data for all fields
    Given Be on Add Vacancy page
    When Enter Vacancy name "<VacancyName>"
    And Select Job Title "<JobTitle>"
    And Enter Description "<Description>"
    And Select Hiring Manger
    And Enter the Number of Positions "<Number>"
    Then Click on Save button (Vacancy page)
    And Confirmation message is displayed (Vacancy page)
    Examples:
      | VacancyName      | JobTitle | Description       | Number |
      | Senior QA Lead85 | QA Lead  | Test Description1 | 1      |

  Scenario: Check if FeedURL and PageURL are valid links opened in new browser tabs
    Given Be on Add Vacancy page
    When Click on FeedURL link
    And Check the FeedURL opened link
    And Click on PageURL link
    Then Check the PageURL opened link

  Scenario Outline: Search in grid by JobTitle and check the search results
    Given Be on Vacancies page
    When Search by a Job Title position <JobTitle>
    And Click on Search button
    Then Check the Search results <JobTitle>
    Examples:
      | JobTitle  |
      | "QA Lead" |

  Scenario Outline: Check the delete all functionality
    Given Be on Vacancies page
    When Search by a Job Title position <JobTitle>
    And Click on Search button
    And Check the Search results <JobTitle>
    And Select all search results
    And Click on Delete Selected button
    Then Confirm the Delete action
    And Check in grid if data is deleted <JobTitle>
    And Check the info message
    Examples:
      | JobTitle  |
      | "QA Lead" |