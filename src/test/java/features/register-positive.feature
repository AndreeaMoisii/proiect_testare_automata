@UI@Positive
Feature: Register positive test scenarios

  Background:
    Given Be on Login page
    When Enter username "Admin"
    And Enter password "admin123"
    Then Click on Login button

  Scenario: Register with mandatory fields
    Given  Add Employee page is opened
    When Enter firstname "Firstname1"
    And Enter lastname "Lastname1"
    And Click on Save button
    Then Confirmation message is displayed

  Scenario: Check if Login Details are kept after a switch off
    Given Add Employee page is opened
    When Switch on Create Login Details section
    And Enter New username "Username2"
    And Select disabled status
    And Switch off Create Login Details section
    And Switch on Create Login Details section
    Then Check if the New username is visible "Username2"
    And Status is disabled




