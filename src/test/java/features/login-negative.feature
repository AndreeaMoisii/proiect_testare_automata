@UI@Negative
Feature: Login negative test scenario

  Scenario Outline: Login with invalid credentials
    Given Be on Login page
    When Enter username "<username>"
    And Enter password "<password>"
    And Click on Login button
    Then Error message is displayed
    Examples:
      | username | password  |
      | Admin    | admin1234 |
