package stepdefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pom.RegisterPage;
import org.junit.Assert;

import static stepdefinition.Hooks.driver;

public class RegisterEmployee {
    RegisterPage registerPage = null;

    @Given("Add Employee page is opened")
    public void addEmployeePageIsOpened() {
        driver.get(RegisterPage.url);
        registerPage = new RegisterPage(driver);
    }

    @When("Enter firstname {string}")
    public void enterFirstnameString(String firstname) {
        registerPage.enterFirstname(firstname);
    }

    @And("Enter lastname {string}")
    public void enterLastname(String lastname) {
        registerPage.enterLastname(lastname);
    }

    @And("Click on Save button")
    public void clickOnSaveButton() {
        registerPage.clickOnSaveButton();
    }

    @Then("Confirmation message is displayed")
    public void confirmationMessageIsDisplayed() {
        Assert.assertTrue("New employee not added", registerPage.confirmationMessage());
    }

    @When("Switch on Create Login Details section")
    public void switchOnCreateLoginDetailsSection() {
        registerPage.switchOnCreateLoginDetails();
    }

    @And("Enter New username {string}")
    public void enterNewUsername(String username) {
        registerPage.enterNewUsername(username);
    }

    @And("Select disabled status")
    public void selectDisabledStatus() {
        registerPage.selectDiabledStatus();
    }

    @And("Switch off Create Login Details section")
    public void switchOffCreateLoginDetailsSection() {
        registerPage.switchOffCreateLoginDetails();
    }

    @Then("Check if the New username is visible {string}")
    public void checkIfTheNewUsernameIsVisible(String Username) {
        Assert.assertEquals("The username input is removed", Username, registerPage.newUsernameIsVisible());
    }

    @And("Status is disabled")
    public void statusIsDisabled() {
        Assert.assertTrue("The account status is enabled", registerPage.selectDiabledStatus());
    }

    @When("Click on Add picture button")
    public void clickOnAddPictureButton() {
        registerPage.addPictureButton();
    }

    @And("Select a picture that exceeds the maximum size allowed")
    public void selectPicture() throws InterruptedException {
        registerPage.uploadPicture();
    }

    @Then("Warning message is displayed")
    public void warningMessageIsDisplayed() {
        Assert.assertEquals("Picture size exceeded", "Attachment Size Exceeded", registerPage.warningMessageForPicture());
    }
}
