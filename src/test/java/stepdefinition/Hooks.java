package stepdefinition;


import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class Hooks {
    protected static WebDriver driver;

    @Before
    public void setup() {
        System.out.println("Setup method");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(60));
    }

    @After
    public void cleanupBrowser() {
        System.out.println("Cleanup method");
        driver.quit();
    }

}
