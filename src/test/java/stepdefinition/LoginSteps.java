package stepdefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pom.LoginPage;
import org.junit.Assert;

import java.time.Duration;

import static stepdefinition.Hooks.driver;

public class LoginSteps {
    LoginPage loginPage = null;

    @Given("Be on Login page")
    public void beOnLoginPage() {
        driver.get(LoginPage.url);
        loginPage = new LoginPage(driver);
    }

    @When("Enter username {string}")
    public void enterUsername(String username) {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
        loginPage.enterUsername(username);
    }

    @And("Enter password {string}")
    public void enterPassword(String password) {
        loginPage.enterPassword(password);
    }

    @And("Click on Login button")
    public void clickOnLoginButton() {
        loginPage.clickLoginButton();
    }

    @Then("User area is displayed")
    public void userAreaIsDisplayed() {
        Assert.assertTrue("The user is logged in", loginPage.userAreaIsDisplayed());
    }

    @Then("Error message is displayed")
    public void errorMessageIsDisplayed() {
        Assert.assertTrue("Invalid username and/or password", loginPage.errorMessageIsDisplayed());
    }

}
