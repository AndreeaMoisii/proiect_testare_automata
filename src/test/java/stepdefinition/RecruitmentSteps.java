package stepdefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pom.RecruitmentPage;

import java.time.Duration;

import static stepdefinition.Hooks.driver;

public class RecruitmentSteps {
    RecruitmentPage recruitmentPage;

    @Given("Be on Add Vacancy page")
    public void addJobVacancyPageIsOpened() {
        driver.get(RecruitmentPage.url);
        recruitmentPage = new RecruitmentPage(driver);
    }

    @When("Enter Vacancy name {string}")
    public void enterVacancyName(String vacancyName) {
        recruitmentPage.enterVacancyName(vacancyName);
    }

    @And("Select Job Title {string}")
    public void selectJobTitle(String string) {
        Assert.assertTrue("Job Title not found", recruitmentPage.selectJobTitle(string));
    }

    @And("Select Hiring Manger")
    public void selectHiringManger() throws InterruptedException {
        Assert.assertTrue("Manager not selected", recruitmentPage.selectHiringManager());
    }

    @And("Enter Description {string}")
    public void enterDescription(String string) {
        recruitmentPage.enterDescription(string);
    }

    @And("Enter the Number of Positions {string}")
    public void enterTheNumberOfPositions(String number) {
        recruitmentPage.enterNumberOfOpenedPositions(number);
    }

    @And("Click on Save button \\(Vacancy page)")
    public void clickOnSaveButtonVacancyPage() {
        recruitmentPage.saveButtonVacancyPage();
    }

    @And("Confirmation message is displayed \\(Vacancy page)")
    public void confirmationMessageIsDisplayedVacancyPage() {
        Assert.assertTrue("Confirmation message not visible", recruitmentPage.confirmationMessageVacancyPage());
    }

    @When("Click on FeedURL link")
    public void clickOnFeedURLLink() {
        recruitmentPage.clickOnLinkFeedURL();
    }

    @And("Click on PageURL link")
    public void clickOnPageURLLink() {
        recruitmentPage.clickOnLinkPageURL();
    }

    @And("Check the FeedURL opened link")
    public void checkTheFeedURLLink() {
        Assert.assertEquals("Broken link", "https://opensource-demo.orangehrmlive.com/web/index.php/recruitmentApply/jobs.rss", driver.getCurrentUrl());
        driver.close();
    }

    @Then("Check the PageURL opened link")
    public void checkThePageURLLink() {
        Assert.assertEquals("Broken link", "https://opensource-demo.orangehrmlive.com/web/index.php/recruitmentApply/jobs.html", driver.getCurrentUrl());
        driver.close();
    }
}
