package stepdefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pom.RecruitmentPage;
import pom.VacanciesPage;

import java.time.Duration;

import static stepdefinition.Hooks.driver;


public class VacanciesSteps {
    VacanciesPage vacanciesPage;

    @Given("Be on Vacancies page")
    public void beOnVacanciesPage() {
        driver.get(VacanciesPage.url);
        vacanciesPage = new VacanciesPage(driver);

    }

    @When("Search by a Job Title position {string}")
    public void searchByAJobTitlePosition(String string) {
        Assert.assertTrue(string + " job title was not be found in drop down list", vacanciesPage.searchByAJobTitlePosition(string));
    }

    @And("Click on Search button")
    public void clickOnSearchButton() {
        vacanciesPage.clickOnSearchButton();
    }

    @And("Select all search results")
    public void clickOnCheckBoxAll() {
        Assert.assertTrue("No records are selected", vacanciesPage.clickOnCheckBoxAll());
    }

    @And("Click on Delete Selected button")
    public void clickOnDeleteSelectedButton() {
        Assert.assertTrue("Delete selected button is not displayed", vacanciesPage.clickOnDeleteSelectedButton());
    }

    @And("Confirm the Delete action")
    public void clickOnConfirmationDeleteButton() {
        vacanciesPage.clickOnDeleteConfirmationButton();
    }

//    @Then("Check the confirmation message")
//    public void displayConfirmationMessage() {
//        Assert.assertFalse("The Confirmation message is not displayed", vacanciesPage.displayConfirmation());
//    }

    @And("Check the info message")
    public void displayInfoMessage() {
        Assert.assertTrue("The Info message is not displayed", vacanciesPage.displayInfoMessage());
    }

    @Then("Check the Search results {string}")
    public void checkTheSearchResults(String string) {
        Assert.assertTrue(string + " value was not found in grid", vacanciesPage.searchInVacanciesGrid(string));
    }


    @Then("Check in grid if data is deleted {string}")
    public void checkInGridIfDataIsDeleted(String string) {
        Assert.assertFalse("The Delete functionality is not working", vacanciesPage.searchInVacanciesGrid(string));
    }
}
